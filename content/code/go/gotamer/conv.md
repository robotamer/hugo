+++
description = ""
date = "2015-01-28T19:54:59Z"
groups = ["gotamer"]
keywords = []
language = "en"
title = "Conv"

+++

Don't use this!!!
=================

Don't even try to learn from it, this was one of my first Go codes, most of it is just plain wrong!

#### Converts from 

 * string to byte 
 * string to int 
 * string to uint
 * int    to string
 * uint   to string
 * byte   to string 
 * byte   to int
 * []byte to string 
 * string to []byte 
 * ...



 * [Pkg Documantation](http://go.pkgdoc.org/bitbucket.org/gotamer/conv "GoTamer Conversion Pkg Documentation")
 * [Repository](https://bitbucket.org/gotamer/conv "GoTamer Conversion Repository")
 

