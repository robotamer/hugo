+++
description = "Network ping for go"
date = "2015-01-28T19:54:59Z"
groups = ["code", "go"]
keywords = ["golang"]
language = "en"
linktitle = "Ping"
tags = ["gotamer"]
title = "Ping"

+++

There are two ways to ping 
 * go internal ping 
 * sys ping 

The go internal ping works only as root, therefor a sys ping is also provided until I can figure out how to fix this problem.

[Website](http://www.robotamer.com "Documentation")

Code is available at <https://bitbucket.org/gotamer>

