+++
description = ""
date = "2015-01-28T19:54:59Z"
groups = ["code", "go"]
keywords = ["code", "go", "gotamer", "random", "string", "shuffle", "reverse"]
linktitle = "Tools"
tags = ["gotamer", "network"]
title = "Your Everyday Go Tools"

+++

Generates a random number between min and max   

	func IRand(min, max int) int


Generates a random strings between min and max   

	func SRand(min, max int, readable bool) string


Reverses the string

	func Reverse(s string) strings


Shuffles the string

	func Shuffle(s string) string

### Links
 * [Pkg Documantation](http://go.pkgdoc.org/bitbucket.org/gotamer/tools "Documentation")
 * [Repository](https://bitbucket.org/gotamer/tools "Repository")



