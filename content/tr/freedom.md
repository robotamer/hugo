+++
date = "2014-12-21T19:50:00Z"
description = "Zihinsel kölelikten Özgürlüye"
groups = ["blog"]
keywords = ["Özgür", "Özgürlük", "Zihinsel kölelik"]
language = "tr"
title = "Özgürlük"

+++

Öğretileni sorgulamadan kabul eden, zihinsel köle olur.  

Zihinsel kölelikten anca kendi kendini azat edebilirsin.  

Zihinsel kölelikten kurtulmanın tek yolu,   
taraf tutmadan her konuya acık yaklaşıp,   
her yönden öğrenmek ve ondan sonra değerlendirmektir.  

Zihinsel kölelikten kendinizi azat etmeden,   
ne özgürlüğün anlamini bilir, nede tadını alabilirsiniz.  

Uyumluluk, özgürlük ve gelişme düşmanıdır.  

Ezen asla özgürlüğünüzü gönüllü vermez,   
zihinsel kölelikten kurtulduktan sonra, zorla alirsiniz.   

Özgürlüğü ile gelen sorumluluğu anlamayan, özgürlüğünü hak etmez.  


