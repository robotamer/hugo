+++
date = "2011-06-29T07:52:39Z"
groups = ["blog"]
language = "en"
tags = ["quote"]
title = "Live without pretending ..."
+++
	
	Live   without pretending .......
	Love   without depending  .......
	Listen without defending  .......
	Speak  without offending  .......

 > by [Drake](http://twitter.com/#!/drakkardnoir)
