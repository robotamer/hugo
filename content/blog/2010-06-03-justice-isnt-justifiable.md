+++
date = "2010-06-03T00:13:05Z"
groups = ["blog"]
language = "en"
tags = ["quotes"]
title = "Justice isn't justifiable"

+++

_The time, effort and money it takes to get justice isn't justifiable!_


> Dennis T Kaplan (June 2nd, 2010)
