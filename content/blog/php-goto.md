+++
date = "2010-03-13T16:15:00Z"
groups = ["blog"]
tags = ["php", "php5"]
title = "PHP goto statement"
+++

I just discovered that the 
[goto statement](http://www.php.net/manual/en/control-structures.goto.php) 
is available since php 5.3  

I love the guys from php for that.  
I am a hobby programmer and unlike the professionals, I learned OOP via php, which is great but having [goto](http://www.php.net/manual/en/control-structures.goto.php) available like back in the Amiga days made my day.
